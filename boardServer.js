let express = require('express');
let bodyParser = require('body-parser');
let fs = require('fs');

let server = express();

server.use(express.static('./html'));
server.use(bodyParser.urlencoded());

// 게시물 목록 화면
server.get('/list', (req, res, next) => {
    // articles.json 파일에서 게시물 목록을 읽어 온다.
    let articles = JSON.parse(fs.readFileSync('./articles.json'));
    // 사용자에게 응답할 html 을 만든다.
    let html = '<html><head><meta charset="UTF-8"><title>Express 게시판</title></head><body>';
    html += '<table width="100%" cellpadding="0" cellspacing="0" class="articles">';
    html += '<tr><th>#</th><th>제목</th><th>작성자</th><th>작성일시</th></tr>';
    for (let i = 0; i < articles.length; i++) {
        html += '<tr><td>'+ (articles.length-i) +'</td><td><a href="/read/'+i+'">'+ articles[i].subject +'</td><td>'+ articles[i].author +'</td><td>'+ articles[i].date +'</td></tr>';
    }
    html += '</table><a href="write.html">글쓰기</a></body></html>';
    res.send(html);
});

// 게시물 작성기능
server.post('/write', (req, res, next) => {
    // JSON 파일에서 게시물 목록을 읽어온다.
    let articles = JSON.parse(fs.readFileSync('./articles.json'));
    // 게시물을 추가한다.
    articles.unshift({subject: req.body.subject, author: req.body.author, content: req.body.content, date: (new Date())});
    // 게시물이 추가된 상태의 JSON 파일을 새로 작성한다.
    fs.writeFileSync('./articles.json', JSON.stringify(articles));
    // 게시물 작성이 완료되면 목록 화면으로 되돌아간다.
    res.redirect('/list');
});

// 게시물 상세보기
server.get('/read/:idx', (req, res, next) => {
    // JSON 파일에서 게시물 목록을 읽어온다.
    let articles = JSON.parse(fs.readFileSync('./articles.json'));
    // 사용자에게 보여줄 게시물을 1개 선택한다.
    let article = articles[req.params.idx];
    // 사용자에게 보여줄 HTML 작성
    console.log(article);
    let html = "<html><head><title>게시판 읽기 화면</title><meta charset='utf-8'>";
    html += "<style>";
    html += "  .article { border:1px solid #cccccc; border-width: 1px 0px 0px 1px; }";
    html += "  .article th, .article td { border:1px solid #cccccc; border-width: 0px 1px 1px 0px; }";
    html += "</style></head><body>"
    html += '<table style="width: 100%" class="article">';
    html += '<tr><th>제목</th><td colspan="5">'+article.subject+'</td></tr>';
    html += "<tr><th>작성자</th><td>"+article.author+"</td><th>작성일시</th><td>"+article.date+"</td><th>조회수</th><td>"+article.hitCount+"</td></tr>";
    html += "<tr><th>내용</th><td colspan='5' style='height:400px;vertical-align:top;'>"+article.content+"</td></tr>";
    html += "</table>";
    html += '<br><a href="/list">목록화면으로 돌아가기</a></body></html>';
    // 응답을 보내준다.
    res.send(html);

});

server.listen(3000, () => {
    console.log('서버의 3000번 PORT 가 준비되었습니다.');
});
