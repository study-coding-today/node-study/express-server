let express = require('express');
// express 객체를 생성하여 할당
let server = express();
// HTTP Request 에 Body 가 있을 때(post 요청) Body 해석해주는 미들웨어
let bodyParer = require('body-parser');

// 01. 미들웨어 정의 (모든 요청에 응답)
/*server.use((req, res, next) => {
    res.send('Hello Express');
});*/

// 02. 미들웨어 정의 (지정한 요청에 응답)
server.use('/sample', (req, res, next) => {
    res.send('Sample Data');
});

// 03. 미들웨어 정의 (get 요청에 응답)
/*server.get('/echo', (req, res, next) => {
    res.send('ECHO');
});*/

// 04. 미들웨어 정의 (index.html 요청에 응답)
/*server.get('/index.html', (req, res, next) => {
    // 응답시 보낼때 경로는 절대경로로 작성한다.
    res.sendFile(__dirname + '/html/index.html');
});*/

// 05. 미들웨어 정의 (모든 file 보내는 폴더 설정)
// static 미들웨어 : 디스크상에 존재하난 파일 요청시 응답
server.use(express.static('./html'));

// 06. 미들웨어 정의 (다음 미들웨어 호출 응답)
// url 일부 내용를 변수로 받아오기
server.get('/bravo/:name', (req, res, next) => {
    // 로그만 남긴다.
    console.log('웹브라우저가 /bravo 요청을 보내왔습니다.');
    // url 에서 받은 name 을 req 의 params 메서드로 확인 할 수 있다.
    console.log('URL 에서 name 변수로 들어온 데이터 : ' + req.params.name);
    next();   // 다음 미들웨어로 넘기기
}, (req, res, next) => {
    res.send('bravo');
});

// HTTP Request 에 Body 가 포함된 경우 req.body 를 만들어주는 미들웨어
server.use(bodyParer.urlencoded());

// 07. 미들웨어 정의 ( get 방식 응답)
/*server.get('/test', (req, res, next) => {
    let html = '<html><head><meta charset="UTF-8"><title>GET or POST 방식의 문서응답</title></head><body>';
    html += '<h2>GET Parameter</h2><br>';
    for (let name in req.query) {
        html += name + ':' + req.query[name] + '<br>';
    }
    html += '</body></html>';
    res.send(html);
});

// 08. 미들웨어 정의 ( post 방식 응답)
server.post('/test', (req, res, next) => {
    let html = '<html><head><meta charset="UTF-8"><title>GET or POST 방식의 문서응답</title></head><body>';
    html += '<h2>POST Parameter</h2><br>';
    for (let name in req.body) {
        html += name + ':' + req.body[name] + '<br>';
    }
    html += '</body></html>';
    res.send(html);
});*/

// 09. 미들웨어 정의 ( get or post 방식 응답)
server.use('/test', (req, res, next) => {
    let html = '<html><head><meta charset="UTF-8"><title>GET or POST 방식의 문서응답</title></head><body>';
    html += '<h2>GET Parameter</h2><br>';
    for (let name in req.query) {
        html += name + ':' + req.query[name] + '<br>';
    }

    html += '<h2>POST Parameter</h2><br>';
    for (let name in req.body) {
        html += name + ':' + req.body[name] + '<br>';
    }
    html += '</body></html>';
    res.send(html);
});


server.listen(3000, () => {
    console.log('웹서버의 3000번 PORT가 준비되었습니다');
});



